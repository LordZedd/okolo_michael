from django.contrib.auth import forms, get_user_model
from django.forms import Form, ModelForm, fields, widgets
from django.utils.translation import ugettext_lazy as _

User = get_user_model()


class RegisterForm1(Form):
    full_name = fields.CharField(label=_('Full name'), max_length=100)
    email = fields.EmailField(label=_('Email address'), max_length=100)
    phone = fields.RegexField(r'\d+', label=_('Phone number'), max_length=20,
        widget=widgets.TextInput(attrs={'placeHolder': _("In international format, without the '+'")}))

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise fields.ValidationError(_('Email address is already in use'), code='duplicate')
        return email

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        if User.objects.filter(phone=phone).exists():
            raise fields.ValidationError(_('Phone number is already in use'), code='duplicate')
        return phone


class RegisterForm2(Form):
    password1 = fields.CharField(label=_('Password'), widget=widgets.PasswordInput)
    password2 = fields.CharField(label=_('Confirm password'), widget=widgets.PasswordInput)


class ProfileForm(ModelForm):
    class Meta:
        model = User
        fields = ('full_name',)


class PasswordChangeForm(forms.PasswordChangeForm):
    """
    Override ``django.contrib.auth.forms.PasswordChangeForm`` to fix stupid constructor argument order.
    """

    def __init__(self, data=None, files=None, instance=None):
        super().__init__(instance, data=data, files=files)


######################################
## Provided for Admin compatibility ##
######################################

class UserCreationForm(forms.UserCreationForm):
    class Meta:
        model = User
        fields = ('full_name', 'email', 'phone',)


class UserChangeForm(forms.UserChangeForm):
    class Meta:
        model = User
        fields = ('full_name', 'email', 'phone',)
