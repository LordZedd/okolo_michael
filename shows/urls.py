from django.conf.urls import include, url
from . import views

app_name = 'shows'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^channel/new$', views.channel, name='new_channel'),
    url(r'^channel/edit$', views.channel, name='manage_channel'),
    url(r'^shows$', views.broadcast, name='broadcast'),
    url(r'^shows/new$', views.show, name='new_show'),
    url(r'^shows/(?P<show_name>[-\w]+)/edit$', views.show, name='manage_show'),
    url(r'^(?P<channel>[-\w]+)/shows/(?P<show_name>[-\w]+)$', views.watch, name='watch'),
]
