from django import forms
from django.forms import widgets
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from .models import Channel, Show


class ChannelForm(forms.ModelForm):
    """Form for creating and updating `Channels`."""

    class Meta:
        model = Channel
        fields = ('name', 'description',)
        widgets = {
            'name': widgets.TextInput(attrs={'placeHolder': 'Provide a unique name for your channel'}),
            'description': widgets.Textarea(attrs={'rows': 4}),
        }

    def __init__(self, *args, **kwds):
        self.request = kwds.pop('request')
        super().__init__(*args, **kwds)
    
    def save(self, commit=True):
        channel = super().save(commit=False)
        if not channel.slug:
            channel.slug = slugify(channel.name)
            channel.owner = self.request.user
        if commit:
            channel.save()
            self.save_m2m()
        return channel


class ShowForm(forms.ModelForm):
    """Form for creating and updating Shows."""

    class Meta:
        model = Show
        fields = ('title', 'description', 'rating', 'age_restricted', 'content_license', 'poster', 'video',)
        widgets = {
            'description': widgets.Textarea(attrs={'rows': 4}),
            'poster': widgets.FileInput(),
            'video': widgets.FileInput(),
        }

    def __init__(self, *args, **kwds):
        self.request = kwds.pop('request')
        super().__init__(*args, **kwds)

    def save(self, commit=True):
        show = super().save(commit=False)
        if not show.pk:
            show.channel = self.request.user.channel
        if commit:
            show.save()
            self.save_m2m()
        return show
